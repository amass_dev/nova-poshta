<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Models;

use Amass\Novaposhta\BaseModel;

/**
 * Class DataContainer
 * @package Amass\Novaposhta\Models
 */
class DataContainer extends BaseModel
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string the model name
     */
    public $modelName;
    /**
     * @var
     */
    public $calledMethod;
    /**
     * @var string API key (sent by the user)
     */
    public $apiKey;
    /**
     * @var \stdClass the name of the called method methodProperties property method for all methods
     */
    public $methodProperties;
    /**
     * @var
     */
    public $language;

}
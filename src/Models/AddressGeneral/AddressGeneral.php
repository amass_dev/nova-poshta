<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Models\AddressGeneral;

use Amass\Novaposhta\Api\ApiMethodInterface;
use Amass\Novaposhta\BaseModel;
use Amass\Novaposhta\MethodProperties\AddressGeneral\AddressGeneralGetSettlements;

class AddressGeneral extends BaseModel
{
    /**
     * @var ApiMethodInterface
     */
    private $api;
    /**
     * Address constructor.
     * @param ApiMethodInterface $api
     */
    public function __construct(ApiMethodInterface $api)
    {
        $this->api = $api;
    }
    /**
     * @param AddressGeneralGetSettlements|null $data
     * @return mixed
     */
    public function getSettlements(AddressGeneralGetSettlements $data = null)
    {
        return $this->api->getResult($this->getCalledModel(), __FUNCTION__, $data);
    }
}
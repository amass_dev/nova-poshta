<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Models;

use Amass\Novaposhta\Core\Base;

/**
 * Class DataContainerResponse
 * @package Amass\Novaposhta\Models
 */
class DataContainerResponse extends Base
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $success;
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var array
     */
    public $errors = [];
    /**
     * @var array
     */
    public $warnings = [];
    /**
     * @var array
     */
    public $info = [];
    /**
     * @var array
     */
    public $messageCodes = [];
    /**
     * @var array
     */
    public $errorCodes = [];
    /**
     * @var array
     */
    public $warningCodes = [];
    /**
     * @var array
     */
    public $infoCodes =[];

    /**
     * DataContainerResponse constructor.
     * @param \stdClass|null $data
     */
    public function __construct(\stdClass $data = null)
    {
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSuccess()
    {
        return $this->success;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getWarnings()
    {
        return $this->warnings;
    }

    /**
     * @return array
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return array
     */
    public function getMessageCodes()
    {
        return $this->messageCodes;
    }

    /**
     * @return array
     */
    public function getErrorCodes()
    {
        return $this->errorCodes;
    }

    /**
     * @return array
     */
    public function getWarningCodes()
    {
        return $this->warningCodes;
    }

    /**
     * @return array
     */
    public function getInfoCodes()
    {
        return $this->infoCodes;
    }
}
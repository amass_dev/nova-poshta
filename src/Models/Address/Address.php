<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Models\Address;

use Amass\Novaposhta\Api\ApiMethodInterface;
use Amass\Novaposhta\BaseModel;
use Amass\Novaposhta\MethodProperties\Address\AddressGetCities;
use Amass\Novaposhta\MethodProperties\Address\AddressGetStreet;

/**
 * Class Address
 * @package Amass\Novaposhta\Models
 * @property string Ref
 * @property string CounterpartyRef
 * @property string StreetRef
 * @property string BuildingRef
 * @property string BuildingNumber
 * @property string Flat
 * @property string Note
 */
class Address extends BaseModel
{
    /**
     * @var ApiMethodInterface
     */
    private $api;
    /**
     * Address constructor.
     * @param ApiMethodInterface $api
     */
    public function __construct(ApiMethodInterface $api)
    {
        $this->api = $api;
    }

    /**
     * @param AddressGetCities|null $data
     * @return mixed
     */
    public function getCities(AddressGetCities $data = null)
    {
        return $this->api->getResult($this->getCalledModel(), __FUNCTION__, $data);
    }

    /**
     * @param AddressGetStreet|null $data
     * @return mixed
     */
    public function getStreet(AddressGetStreet $data = null)
    {
        return $this->api->getResult($this->getCalledModel(), __FUNCTION__, $data);
    }
}
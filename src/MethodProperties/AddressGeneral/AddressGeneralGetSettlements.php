<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\MethodProperties\AddressGeneral;

use Amass\Novaposhta\MethodProperties\MethodProperties;
/**
 * Class AddressGeneralGetSettlements
 * @package Amass\Novaposhta\MethodProperties
 */
class AddressGeneralGetSettlements extends MethodProperties
{
    /**
     * @var string
     */
    public $Ref;
    /**
     * @var string
     */
    public $FindByString;
    /**
     * @var string
     */
    public $Page;
    /**
     * @var string
     */
    public $MainCitiesOnly;
    /**
     * @var string
     */
    public $HideMainCities;
    /**
     * @var string
     */
    public $RegionRef;
    /**
     * @var string
     */
    public $AreaRef;

    /**
     * AddressGetCities constructor.
     * @param string $FindByString
     */
    public function __construct($FindByString = '')
    {
        $this->FindByString = $FindByString;
    }
    /**
     *
     * @return string
     */
    public function getRef()
    {
        return $this->Ref;
    }

    /**
     * @param $Ref
     * @return $this
     */
    public function setRef($Ref)
    {
        $this->Ref = $Ref;
        return $this;
    }
    /**
     *
     * @return string
     */
    public function getFindByString()
    {
        return $this->FindByString;
    }

    /**
     * @param $FindByString
     * @return $this
     */
    public function setFindByString($FindByString)
    {
        $this->FindByString = $FindByString;
        return $this;

    }
    /**
     *
     * @return string
     */
    public function getPage()
    {
        return $this->Page;
    }

    /**
     * @param $Page
     * @return $this
     */
    public function setPage($Page)
    {
        $this->Page = $Page;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getMainCitiesOnly()
    {
        return $this->MainCitiesOnly;
    }

    /**
     * @param $MainCitiesOnly
     * @return $this
     */
    public function setMainCitiesOnly($MainCitiesOnly)
    {
        $this->MainCitiesOnly = $MainCitiesOnly;
        return $this;
    }
    /**
     *
     * @return string
     */
    public function getHideMainCities()
    {
        return $this->HideMainCities;
    }

    /**
     * @param $HideMainCities
     * @return $this
     */
    public function setHideMainCities($HideMainCities)
    {
        $this->HideMainCities = $HideMainCities;
        return $this;

    }
    /**
     *
     * @return string
     */
    public function getRegionRef()
    {
        return $this->RegionRef;
    }

    /**
     * @param $RegionRef
     * @return $this
     */
    public function setRegionRef($RegionRef)
    {
        $this->RegionRef = $RegionRef;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getAreaRef()
    {
        return $this->AreaRef;
    }

    /**
     * @param $AreaRef
     * @return $this
     */
    public function setAreaRef($AreaRef)
    {
        $this->AreaRef = $AreaRef;
        return $this;
    }
}
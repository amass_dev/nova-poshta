<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\MethodProperties;

use Amass\Novaposhta\BaseModel;

/**
 * Class MethodProperties
 * @package Amass\Novaposhta\MethodProperties
 */
abstract class MethodProperties extends BaseModel implements MethodPropertiesInterface
{

}
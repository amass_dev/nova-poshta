<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\MethodProperties\Address;

/**
 * Class AddressGetStreet
 * @package Amass\Novaposhta\MethodProperties
 */
use Amass\Novaposhta\MethodProperties\MethodProperties;

class AddressGetStreet extends MethodProperties
{
    /**
     * @var string
     */
    public $CityRef;
    /**
     * @var string|null
     */
    public $FindByString;
    /**
     * @var integer|null
     */
    public $Page;

    /**
     * AddressGetStreet constructor.
     * @param string $CityRef
     */
    public function __construct($CityRef)
    {
        $this->CityRef = $CityRef;
    }

    /**
     *
     * @return string
     */
    public function getCityRef()
    {
        return $this->CityRef;
    }
    /**
     *
     * @return string
     */
    public function getFindByString()
    {
        return $this->FindByString;
    }

    /**
     * @param $FindByString
     * @return $this
     */
    public function setFindByString($FindByString)
    {
        $this->FindByString = $FindByString;
        return $this;
    }
    /**
     *
     * @return string
     */
    public function getPage()
    {
        return $this->Page;
    }

    /**
     * @param $Page
     * @return $this
     */
    public function setPage($Page)
    {
        $this->Page = $Page;
        return $this;
    }
}
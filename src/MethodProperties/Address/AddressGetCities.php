<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\MethodProperties\Address;
/**
 * Class AddressGetCities
 * @package Amass\Novaposhta\MethodProperties
 * @property string Ref
 * @property string FindByString
 * @property string Page
 */
use Amass\Novaposhta\MethodProperties\MethodProperties;

class AddressGetCities extends MethodProperties
{
    /**
     * @var
     */
    public $Ref;
    /**
     * @var
     */
    public $FindByString;
    /**
     * @var
     */
    public $Page;

    /**
     * AddressGetCities constructor.
     * @param string $FindByString
     */
    public function __construct($FindByString = '')
    {
        $this->FindByString = $FindByString;
    }
    /**
     *
     * @return string
     */
    public function getRef()
    {
        return $this->Ref;
    }

    /**
     * @param $Ref
     * @return $this
     */
    public function setRef($Ref)
    {
        $this->Ref = $Ref;
        return $this;
    }
    /**
     *
     * @return string
     */
    public function getFindByString()
    {
        return $this->FindByString;
    }

    /**
     * @param $FindByString
     * @return $this
     */
    public function setFindByString($FindByString)
    {
        $this->FindByString = $FindByString;
        return $this;

    }
    /**
     *
     * @return string
     */
    public function getPage()
    {
        return $this->Page;
    }

    /**
     * @param $Page
     * @return $this
     */
    public function setPage($Page)
    {
        $this->Page = $Page;
        return $this;
    }
}
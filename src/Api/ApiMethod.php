<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Api;

use Amass\Novaposhta\BaseModel;
use Amass\Novaposhta\Core\Config;
use Amass\Novaposhta\Exceptions\NovaPoshtaBaseException;
use Amass\Novaposhta\Logger\Logger;
use Amass\Novaposhta\Models\DataContainer;
use Amass\Novaposhta\Models\DataContainerResponse;
use Amass\Novaposhta\Serializer\SerializerFactory;
use Amass\Novaposhta\Serializer\SerializerInterface;

/**
 * Class ApiMethod
 * @package Amass\Novaposhta\Api
 */
class ApiMethod extends BaseModel implements ApiMethodInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var mixed
     */
    private $logger;
    /**
     * ApiMethod constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->logger = $config->getLogger();
        if (null === $this->logger){
            $this->logger = new Logger();
        }
        $this->serializer = (new SerializerFactory($config->getFormat()))->getSerializer();
    }

    public function getResult($modeName, $calledMethod, $methodProperties)
    {
        $dataContainer = new DataContainer();
        $dataContainer->modelName = $modeName;
        $dataContainer->calledMethod = $calledMethod;
        $dataContainer->methodProperties = $methodProperties;

        return $this->send($dataContainer);
    }

    /**
     * Build API Method URL
     *
     * @return string API Method URL
     */
    private function buildMethodApiUrl()
    {
        return $this->config->getUrlApi();
    }

    /**
     * Отправить запрос
     *
     * @param DataContainer $dataContainer
     * @return bool|int|mixed|DataContainerResponse
     * @throws NovaPoshtaBaseException
     */
    private function send(DataContainer $dataContainer)
    {
        $dataContainer->apiKey = $this->config->getApiKey();
        $dataContainer->language = $this->config->getLanguage();
        $dataContainer->id = uniqid();

        $this->logger->setOriginalToData($dataContainer);
        $data = $this->serializer->serializeData($dataContainer);
        $this->logger->setToData($data);
        $response = $this->query($data);
        $this->logger->setFromData($response);
        if($response){
            $response = $this->serializer->unserializeData($response);
        } else {
            $response = new DataContainerResponse();
            $response->success = false;
            $response->errors[] = array('DataSerializerJSON.ERROR_REQUEST');
        }
        $this->logger->setOriginalFromData($response);

        return $response;
    }


    /**
     * @param $data
     * @return bool|mixed
     */
    private function query($data)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->buildMethodApiUrl());
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/plain"));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }
}
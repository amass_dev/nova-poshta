<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Api;
use Amass\Novaposhta\BaseModel;
use Amass\Novaposhta\Client;
use Amass\Novaposhta\Core\Config;
use Amass\Novaposhta\MethodProperties\MethodPropertiesInterface;

/**
 * Interface ApiMethodInterface
 * @package Amass\Novaposhta\Api
 */

interface ApiMethodInterface
{
    /**
     * ApiMethodInterface constructor.
     * @param Config $config
     */
    public function __construct(Config $config);

    /**
     * @param string $modelName
     * @param $calledMethod
     * @param MethodPropertiesInterface $methodProperties
     * @return mixed
     */
    public function getResult($modelName, $calledMethod, $methodProperties);
}
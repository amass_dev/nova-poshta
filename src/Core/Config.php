<?php
/**
 * This file is part of the "Nova Poshta" 2.0 API PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Core;
use Amass\Novaposhta\Logger\LoggerInterface;

/**
 * Class Config
 * @package Amass\personal Manager or customer\Core
 */
class Config
{
    /**
     * The format of the data in the JSON format
     * @see https://api.novaposhta.ua/v2.0/json/
     */
    const FORMAT_JSON = 'json';
    /**
     * The format of the data in the format JSONRPC2
     * @see  https://api.novaposhta.ua/v2.0/jsonrpc2
     */
    const FORMAT_JSONRPC2 = 'jsonrpc2';
    /**
     * The format of the data in XML format
     * @see  https://api.novaposhta.ua/v2.0/xml/
     */
    const FORMAT_XML = 'xml';
    /**
     * Return data in Russian language
     */
    const LANGUAGE_UA = 'ua';
    /**
     * Return data in Russian language with API of New mail (if foreseen to API of New mail)
     */
    const LANGUAGE_RU = 'ru';
    /**
     * To return the information in English-APIs for New mail (if foreseen to API of New mail)
     */
    const LANGUAGE_EN = 'en';
    /**
     * @var string API key
     */
    private $apiKey;
    /**
     * @var string the default format for data transfer
     */
    private $format = self::FORMAT_JSON;
    /**
     * @var string the default language for data transmission
     */
    private $language = self::LANGUAGE_UA;
    /**
     * @var string URL API2
     */
    private $urlApi = 'https://api.novaposhta.ua/v2.0/';
    /**
     * @var string URL the personal account for New Mail
     */
    private $urlMyNovaPoshta = 'https://my.novaposhta.ua';

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Returns format of data transmission
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Sets transmission format
     *
     * @param string $value
     * @return self
     */
    public function setFormat($value)
    {
        $this->format = $value;
        return $this;
    }

    /**
     * Returns the language for the response with a New email
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets the language for response with a New email
     *
     * @param string $value
     * @return self
     */
    public function setLanguage($value)
    {
        $this->language = $value;
        return $this;
    }

    /**
     * Returns the URL API 2
     *
     * @return string
     */
    public function getUrlApi()
    {
        return $this->urlApi . $this->format . '/';
    }

    /**
     * Returns the URL of the New office Mail
     *
     * @return string
     */
    public function getUrlMyNovaPoshta()
    {
        return $this->urlMyNovaPoshta;
    }

    /**
     * @param LoggerInterface $logger
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
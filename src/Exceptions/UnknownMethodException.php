<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Exceptions;

/**
 * Class UnknownMethodException
 * @package Amass\Novaposhta\Exceptions
 */
class UnknownMethodException extends NovaPoshtaBaseException
{

}
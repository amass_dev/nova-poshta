<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Serializer;

use Amass\Novaposhta\Models\DataContainer;
use Amass\Novaposhta\Models\DataContainerResponse;

/**
 * Class SerializeJSON
 * @package Amass\Novaposhta\Serialize
 */
class SerializerJSON implements SerializerInterface
{
    /**
     * @param DataContainer $data
     * @return string
     */
    public function serializeData(DataContainer $data)
    {
        return json_encode($data);
    }

    /**
     * @param string $json
     * @return DataContainerResponse
     */
    public function unserializeData($json)
    {
        $data = (array)json_decode($json);
        if (json_last_error() != JSON_ERROR_NONE) {
            $dataContainerResponse = new DataContainerResponse();
            $dataContainerResponse->success = false;
            $dataContainerResponse->errors[] = array('DataSerializerJSON.DATA_IS_INVALID');
            return $dataContainerResponse;
        }
        $data = json_decode(json_encode($data), false);
        $data->errors = (array)$data->errors;
        $dataContainerResponse = new DataContainerResponse($data);
        return $dataContainerResponse;
    }
}
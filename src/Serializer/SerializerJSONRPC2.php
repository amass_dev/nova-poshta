<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Serializer;

use Amass\Novaposhta\Core\Base;
use Amass\Novaposhta\Models\DataContainer;
use Amass\Novaposhta\Models\DataContainerResponse;

/**
 * Class SerializerJSONRPC2
 * @package Amass\Novaposhta\Serializer
 */
class SerializerJSONRPC2 extends Base implements SerializerInterface
{
    /**
     * @param DataContainer $data
     * @return string
     */
    public function serializeData(DataContainer $data)
    {
        $dataJSONRPC2 = $this->dataContainer2dataSerializerJSONRPC2(array($data));
        $json = json_encode($dataJSONRPC2[0]);
        return $json;
    }

    /**
     * @param string $json
     * @return mixed
     */
    public function unserializeData($json)
    {
        $data = json_decode($json);
        $dataContainer = $this->dataSerializerJSONRPC2dataContainer(array($data));
        return $dataContainer[0];
    }

    /**
     * @param array $dataItems
     * @return array
     */
    private function dataContainer2dataSerializerJSONRPC2(array $dataItems)
    {
        $data = array();
        foreach($dataItems as $item){
            $dataJSONRPC2 = new \stdClass();
            $dataJSONRPC2->id = $item->id;
            $dataJSONRPC2->jsonrpc = '2.0';
            $dataJSONRPC2->method = $item->modelName . '.' . $item->calledMethod;
            $dataJSONRPC2->params = new \stdClass();
            $dataJSONRPC2->params->methodProperties = $item->methodProperties;
            $dataJSONRPC2->params->language = $item->language;
            $dataJSONRPC2->params->apiKey = $item->apiKey;
            $data[] = $dataJSONRPC2;
        }
        return $data;
    }

    /**
     * @param $dataItems
     * @return DataContainerResponse|array
     */
    private function dataSerializerJSONRPC2dataContainer($dataItems)
    {
        $dataContainers = array();
        foreach($dataItems as $data){
            if (json_last_error() != JSON_ERROR_NONE) {
                $dataContainerResponse = new DataContainerResponse();
                $dataContainerResponse->success = false;
                $dataContainerResponse->errors[] = array('SerializerJSONRPC2.DATA_IS_INVALID');
                return $dataContainerResponse;
            }
            $dataContainer = new DataContainerResponse();
            $dataContainer->success = isset($data->result) ? true : false;
            if ($dataContainer->success) {
                $dataContainer->data = $data->result;
            } else {
                if(isset($data->error->data)){
                    $dataContainer->errors = (array)$data->error->data;
                } else {
                    $dataContainer->errors = $data->errors;
                }
            }
            $dataContainer->warnings = $data->warnings;
            $dataContainer->info = $data->info;
            if (isset($data->id)){
                $dataContainer->id = $data->id;
            }
            $dataContainers[] = $dataContainer;
        }
        return $dataContainers;
    }
}
<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Serializer;

use Amass\Novaposhta\Exceptions\InvalidClassException;

/**
 * Class SerializerFactory
 * @package Amass\Novaposhta\Serializer
 */
class SerializerFactory
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var string
     */
    private $format;

    /**
     * SerializerFactory constructor.
     * @param $format
     */
    public function __construct($format)
    {
        $this->format =  $format;
    }

    /**
     * @return SerializerInterface|SerializerFactory
     * @throws InvalidClassException
     */
    public function getSerializer()
    {
        $dataSerializer = '\Amass\Novaposhta\Serializer\Serializer' . strtoupper($this->format);
        if (!class_exists($dataSerializer)) {
            throw new InvalidClassException('Not found serializer ' . $dataSerializer);
        }
        if (!$this->serializer) {
            $this->serializer = new $dataSerializer();
        }

        return $this->serializer;
    }
}
<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Serializer;

/**
 * Interface SerializerInterface
 * @package Amass\Novaposhta\Serializer
 */

use Amass\Novaposhta\Models\DataContainer;

interface SerializerInterface
{
    /**
     * @param DataContainer $data
     * @return mixed
     */
    public function serializeData(DataContainer $data);
    /**
     *
     * @param string $data
     * @return mixed
     */
    public function unserializeData($data);
}
<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Logger;

use Amass\Novaposhta\Models\DataContainer;
use Amass\Novaposhta\Models\DataContainerResponse;

/**
 * Interface LoggerInterface
 * @package Amass\Novaposhta\Logger
 */
interface LoggerInterface
{
    /**
     * @param $toData
     * @return mixed
     */
    public function setToData($toData);

    /**
     * @param $fromData
     * @return mixed
     */
    public function setFromData($fromData);

    /**
     * @param DataContainer $toData
     * @return mixed
     */
    public function setOriginalToData(DataContainer $toData);

    /**
     * @param DataContainerResponse $fromData
     * @return mixed
     */
    public function setOriginalFromData(DataContainerResponse $fromData);
}
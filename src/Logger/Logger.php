<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Logger;

use Amass\Novaposhta\Models\DataContainer;
use Amass\Novaposhta\Models\DataContainerResponse;

class Logger implements LoggerInterface
{
    public function setOriginalToData(DataContainer $toData)
    {
        // TODO: Implement setOriginalToData() method.
    }

    public function setOriginalFromData(DataContainerResponse $fromData)
    {
        // TODO: Implement setOriginalFromData() method.
    }

    public function setToData($toData)
   {
       // TODO: Implement setToData() method.
   }

   public function setFromData($fromData)
   {
       // TODO: Implement setFromData() method.
   }
}
<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\MethodProperties;

use Amass\Novaposhta\MethodProperties\AddressGeneral\AddressGeneralGetSettlements;

class AddressGeneralGetSettlementsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AddressGeneralGetSettlements
     */
    private $addressGeneralGetSettlements;

    public function setUp()
    {
        $this->addressGeneralGetSettlements = new AddressGeneralGetSettlements('string');
        $this->addressGeneralGetSettlements->setFindByString('string');
        $this->addressGeneralGetSettlements->setRef('12345-12345-12345');
        $this->addressGeneralGetSettlements->setPage(1);
        $this->addressGeneralGetSettlements->setMainCitiesOnly(null);
        $this->addressGeneralGetSettlements->setHideMainCities(null);
        $this->addressGeneralGetSettlements->setAreaRef(null);
        $this->addressGeneralGetSettlements->setRegionRef(null);
        $this->addressGeneralGetSettlements->setAreaRef(null);
        parent::setUp();
    }

    public function testCheckProperties()
    {
        $this->assertObjectHasAttribute('Ref', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('Page', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('FindByString', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('MainCitiesOnly', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('HideMainCities', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('RegionRef', new AddressGeneralGetSettlements);
        $this->assertObjectHasAttribute('AreaRef', new AddressGeneralGetSettlements);
    }

    public function testGetCityRef()
    {
        $this->assertEquals('12345-12345-12345', $this->addressGeneralGetSettlements->getRef());
    }

    public function testGetPage()
    {
        $this->assertEquals(1, $this->addressGeneralGetSettlements->getPage());
    }

    public function testFindByString()
    {
        $this->assertEquals('string', $this->addressGeneralGetSettlements->getFindByString());
    }


    public function testGetMainCitiesOnly()
    {
        $this->assertEquals(null, $this->addressGeneralGetSettlements->getMainCitiesOnly());
    }

    public function testGetHideMainCities()
    {
        $this->assertEquals(null, $this->addressGeneralGetSettlements->getHideMainCities());
    }

    public function testGetRegionRef()
    {
        $this->assertEquals(null, $this->addressGeneralGetSettlements->getRegionRef());
    }

    public function testGetAreaRef()
    {
        $this->assertEquals(null, $this->addressGeneralGetSettlements->getAreaRef());
    }
}

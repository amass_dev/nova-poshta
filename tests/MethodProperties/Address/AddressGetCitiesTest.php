<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\MethodProperties;

use Amass\Novaposhta\MethodProperties\Address\AddressGetCities;

class AddressGetCitiesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AddressGetCities
     */
    private $addressGetCities;

    public function setUp()
    {
        $this->addressGetCities = new AddressGetCities('string');
        $this->addressGetCities->setRef('12345');
        $this->addressGetCities->setFindByString('string');
        $this->addressGetCities->setPage(1);
        parent::setUp();
    }

    public function testCheckProperties()
    {
        $this->assertObjectHasAttribute('Ref', new AddressGetCities());
        $this->assertObjectHasAttribute('Page', new AddressGetCities());
        $this->assertObjectHasAttribute('FindByString', new AddressGetCities());
    }
    public function testGetRef()
    {
        $this->assertEquals('12345', $this->addressGetCities->getRef());
    }

    public function testGetPage()
    {
        $this->assertEquals(1, $this->addressGetCities->getPage());
    }

    public function testGetFindByString()
    {
        $this->assertEquals('string', $this->addressGetCities->getFindByString());
    }
}

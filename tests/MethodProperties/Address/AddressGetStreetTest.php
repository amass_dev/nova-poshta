<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\MethodProperties;

use Amass\Novaposhta\MethodProperties\Address\AddressGetStreet;

class AddressGetStreetTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AddressGetStreet
     */
    private $addressGetStreet;

    public function setUp()
    {
        $this->addressGetStreet = new AddressGetStreet('12345-12345-12345');
        $this->addressGetStreet->setFindByString('string');
        $this->addressGetStreet->setPage(1);
        parent::setUp();
    }

    public function testCheckProperties()
    {
        $this->assertObjectHasAttribute('CityRef', new AddressGetStreet('12345-12345-12345'));
        $this->assertObjectHasAttribute('Page', new AddressGetStreet('12345-12345-12345'));
        $this->assertObjectHasAttribute('FindByString', new AddressGetStreet('12345-12345-12345'));
    }

    public function testGetCityRef()
    {
        $this->assertEquals('12345-12345-12345', $this->addressGetStreet->getCityRef());
    }

    public function testGetPage()
    {
        $this->assertEquals(1, $this->addressGetStreet->getPage());
    }

    public function testGetFindByString()
    {
        $this->assertEquals('string', $this->addressGetStreet->getFindByString());
    }
}

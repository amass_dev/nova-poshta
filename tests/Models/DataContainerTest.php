<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\Models;

use Amass\Novaposhta\Models\DataContainer;

class DataContainerTest extends \PHPUnit_Framework_TestCase
{

    public function testCheckProperties()
    {
        $this->assertObjectHasAttribute('id', new DataContainer());
        $this->assertObjectHasAttribute('modelName', new DataContainer());
        $this->assertObjectHasAttribute('calledMethod', new DataContainer());
        $this->assertObjectHasAttribute('apiKey', new DataContainer());
        $this->assertObjectHasAttribute('methodProperties', new DataContainer());
        $this->assertObjectHasAttribute('language', new DataContainer());

    }
}

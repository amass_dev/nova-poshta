<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\Models;

use Amass\Novaposhta\Models\DataContainerResponse;

class DataContainerResponseTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var DataContainerResponse
     */
    private $container;
    public function setUp()
    {
        $data = new \stdClass();
        $data->id = '1';
        $data->success = 1;
        $data->data = [0 => 'SomeData'];
        $data->errors = [];
        $this->container = new DataContainerResponse($data);

        parent::setUp();
    }

    public function testCheckProperties()
    {
        $this->assertObjectHasAttribute('id', new DataContainerResponse());
        $this->assertObjectHasAttribute('success', new DataContainerResponse());
        $this->assertObjectHasAttribute('data', new DataContainerResponse());
        $this->assertObjectHasAttribute('errors', new DataContainerResponse());
        $this->assertObjectHasAttribute('warnings', new DataContainerResponse());
        $this->assertObjectHasAttribute('info', new DataContainerResponse());
        $this->assertObjectHasAttribute('messageCodes', new DataContainerResponse());
        $this->assertObjectHasAttribute('errorCodes', new DataContainerResponse());
        $this->assertObjectHasAttribute('warningCodes', new DataContainerResponse());
        $this->assertObjectHasAttribute('infoCodes', new DataContainerResponse());
    }

    public function testAddData()
    {
        $this->assertEquals('1', $this->container->getId());
        $this->assertEquals(1, $this->container->getSuccess());
        $this->assertEquals([0 => 'SomeData'], $this->container->getData());
        $this->assertArrayHasKey(0, $this->container->getData());
        $this->assertEquals([], $this->container->getErrors());
    }
}

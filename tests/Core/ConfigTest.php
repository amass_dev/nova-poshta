<?php
/**
 * This file is part of the "Nova Poshta" API 2.0 PHP Client
 *
 * @copyright 2016 AmassDevelopment
 * @link http//www.amass.pp.ua
 * @author Igor Moskal <amassDev@gmail.com>
 *
 * @license GNU GPL v.3
 */

namespace Amass\Novaposhta\Tests\Core;

use Amass\Novaposhta\Core\Config;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigTest
 */
class ConfigTest extends TestCase
{
    /**
     * @var Config
     */
    private $config;
    public function setUp()
    {
        $this->config = new Config($GLOBALS['apiKey']);
        $this->config->setFormat(Config::FORMAT_JSON);
        $this->config->setLanguage(Config::LANGUAGE_UA);

        parent::setUp();
    }

    public function testGetApiKey()
    {
        $this->assertEquals($GLOBALS['apiKey'], $this->config->getApiKey());
    }

    public function testGetFormat()
    {
        $this->assertEquals(Config::FORMAT_JSON, $this->config->getFormat());
    }

    public function testGetLanguage()
    {
        $this->assertEquals(Config::LANGUAGE_UA, $this->config->getLanguage());
    }

    public function testGetUri()
    {
        $this->assertEquals('https://api.novaposhta.ua/v2.0/' . Config::FORMAT_JSON . '/', $this->config->getUrlApi());
    }
}
